import React, { useState } from 'react'

const useDnD = (initialState) => {
    const [dndState, setDndState] = useState(initialState);

    return dndState
}

export default useDnD