import React, { useState } from 'react';
import ImageDragDropView from './components/view/ImageDragDropView';
import './App.scss'

function App() {
  return (
    <div className="App">
      <ImageDragDropView></ImageDragDropView>
    </div>
  )
}

export default App
