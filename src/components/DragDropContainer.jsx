import React, { useState } from 'react';
import { useDrop, useDragDropManager } from 'react-dnd';
import DragContainer from './DragContainer';
import DraggableItem from './DraggableItem';
import DropArea from './DropArea';
import useDnD from '../useDnD';

const dragData = [
    {
        id: '0',
        title: "Title 01",
        url: "http://placeimg.com/240/180/arch"
    },
    {
        id: '1',
        title: "Title 02",
        url: "http://placeimg.com/240/180/arch"
    },
    {
        id: '2',
        title: "Title 03",
        url: "http://placeimg.com/240/180/arch"
    },
    {
        id: '3',
        title: "Title 04",
        url: "http://placeimg.com/240/180/arch"
    }
]

const DragDropContainer = () => {
    const [questions, setQuestions] = useState(dragData)
    const [synonyms, setSynonyms] = useState([])
    const [antonyms, setAntonyms] = useState([])
    const [dropState, dropRef] = useDrop(() => ({ accept: 'image' }))
    const dragDropManager = useDragDropManager()


    const onSynonymDrop = (item, items) => {
        setQuestions(state => {
            const itemIndex = state.findIndex(i => i.id === item.id);
            const newState = [...state];

            // if (itemIndex >= 0) {
            //     newState[itemIndex].dropped = true
            //     return newState
            // }
            return [...state.filter(i => i.id !== item.id)]
        })

        setSynonyms(state => {
            return [...state, item]
        })
    }

    return (
        <div className="drag-drop-container">
            <div className="drag-container">
                {questions.map(item => (
                    <div className="drag-wrapper">
                        <DraggableItem data={item}></DraggableItem>
                    </div>
                ))}
            </div>

            <div className="drop-container">
                <DropArea data={synonyms} onDropped={onSynonymDrop} id="Synonyms"></DropArea>
                <DropArea data={antonyms} onDropped={onSynonymDrop} id="Antonyms"></DropArea>
            </div>
        </div>
    );
};

export default DragDropContainer;