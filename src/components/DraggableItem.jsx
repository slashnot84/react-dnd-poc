import React from 'react';
import { useDrag, useDrop } from 'react-dnd';


const DraggableItem = ({ data }) => {
    const [dragState, dragRef, dragPreviewRef] = useDrag(
        () => ({
            type: "image",
            item: data,
            collect: (monitor) => {
                return {
                    isDragging: monitor.isDragging(),
                    items: monitor.getItem()
                }
            },
            end: (item, monitor) => {  
            }
        }),
        [data]
    )

    const [dropState, dropRef] = useDrop(() => ({ accept: 'image' }))

    return (
        <div ref={dragRef} className="image-dnd">
            {data && <h3>{data.id} - {data.title}</h3>}
        </div>
    )
};

export default DraggableItem;