import React from 'react';
import { useDrag } from 'react-dnd';


const ImageDragDrop = () => {
    const [{ opacity }, dragRef] = useDrag(
        () => ({
            type: "image",
            item: { text: "Draggable Image" },
            collect: (monitor) => ({
                opacity: monitor.isDragging() ? 0.5 : 1
            })
        }),
        []
    )

    return (
        <div ref={dragRef} className="image-dnd">
            <h3>Image Drag and Drop</h3>
        </div>
    );
};

export default ImageDragDrop;