import React from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import DragDropContainer from '../DragDropContainer';
import './ImageDragDropView.scss';

const ImageDragDropView = () => {
    return (
        <div className="image-drag-drop-view">
            <DndProvider backend={HTML5Backend}>
                <div className="view-header">
                    <h2>Image Drag and Drop</h2>
                    <h3>Test User 3</h3>
                </div>
                <div className="view-container">
                    <DragDropContainer></DragDropContainer>
                </div>
            </DndProvider>
        </div>
    );
};

export default ImageDragDropView;