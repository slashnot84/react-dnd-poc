import React, { useState, useRef } from 'react';
import { useDrop } from 'react-dnd';
import DraggableItem from './DraggableItem'

const DropArea = ({ accept = "image", data = [], onDropped, children, name, id }) => {
    const [droppedItems, setDroppedItems] = useState(data)
    const elmRef = useRef(null)

    const [dropState, dropRef] = useDrop(() => ({
        accept: [accept],
        drop: (item) => {
            // setDroppedItems(dropped => {
            //     return [...dropped, item]
            // });

            onDropped && onDropped(item, [...droppedItems, item])

            return item
        },
        collect: (monitor) => {
            return {
                isOver: monitor.isOver(),
                item: monitor.getItem(),
                didDrop: monitor.didDrop(),
                result: monitor.getDropResult()
            }
        },
    }), [data])

    return (
        <div ref={dropRef} className={`drop-zone ${dropState.isOver && 'is-over'}`}>
            {data.map((item, i) => (
                // <div key={i} className="drop-item">
                //     {item.id}
                // </div>
                <DraggableItem key={i} data={item}></DraggableItem>
            ))}
            {console.log(dropState)}
        </div>
    )
};

export default DropArea;